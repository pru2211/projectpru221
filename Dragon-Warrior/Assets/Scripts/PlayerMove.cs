using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] private float speed;
    private Rigidbody2D body;
    private Animator animator;
    private bool grounded;
    // Start is called before the first frame update
    void Start()
    {
        //Grab references for rigidbody and animetor from object
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }
    //private void Awake()
    // Update is called once per frame
    void Update()
    {
        float horizontalInput = Input.GetAxis("Horizontal");
        body.velocity = new Vector2(horizontalInput * speed, body.velocity.y);

        //flip player when moving left-rignt
        if (horizontalInput > 0.01f)
            transform.localScale = Vector3.one;
        else if(horizontalInput< -0.01f){
            transform.localScale = new Vector3(-1,1,1);

        }

        if (Input.GetKey(KeyCode.Space) && grounded)
            Jump();
        
        //set animatior parameters
        animator.SetBool("run", horizontalInput != 0);
        animator.SetBool("grounded", grounded);
    }
    private void Jump() {
        body.velocity = new Vector2(body.velocity.x, speed);
        animator.SetTrigger("jump");
        grounded = false;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag == "Ground")
            grounded = true;
    }
}




